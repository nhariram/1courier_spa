import Vue from 'vue'
import Router from 'vue-router'
// import Dashboard from '@/components/Dashboard'
import Login from '@/components/Login/Login'
import Dashboard from '@/components/Dashboard'
import CreateOrder from '@/components/CreateOrder/CreateOrder'
import ManageOrders from '@/components/ManageOrders/ManageOrders'
import OrderStatus from '@/components/OrderStatus/OrderStatus'
import Invoice from '@/components/Invoice/Invoice'

Vue.use(Router)
// export default
// https://github.com/manojkumar3692/Vuejs-Authentication/tree/master/LB2/src
const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'dashboard',
      component: Dashboard,
      meta: { requiresAuth: true },
      redirect: 'createorder',
      children: [
        {
          path: 'createorder',
          name: 'createorder',
          component: CreateOrder,
          meta: { requiresAuth: true, residentAuth: true }
        },
        {
          path: 'manageorders',
          name: 'manageorders',
          component: ManageOrders,
          meta: { requiresAuth: true, residentAuth: true }
        },
        {
          path: 'orderstatus/:id',
          name: 'orderstatus',
          component: OrderStatus,
          meta: { requiresAuth: true, residentAuth: true }
        },
        {
          path: 'invoice',
          name: 'invoice',
          component: Invoice,
          meta: { requiresAuth: true, residentAuth: true }
        }
      ]
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (to.meta.requiresAuth) {
    const authUser = JSON.parse(window.localStorage.getItem('lbUser'))
    if (!authUser || !authUser.token) {
      next({name: 'login'})
    } else if (to.meta.adminAuth) {
      const authUser = JSON.parse(window.localStorage.getItem('lbUser'))
      if (authUser.data.role_id === 'ADMIN') {
        next()
      } else {
        next('/resident')
      }
    } else if (to.meta.residentAuth) {
      const authUser = JSON.parse(window.localStorage.getItem('lbUser'))
      if (authUser.data.role_id === 'RESIDENT') {
        next()
      } else {
        console.log('Im in admin')
        next('/admin')
      }
    }
  } else {
    next()
  }
})

export default router
