// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import vmodal from 'vue-js-modal'
import VueGoodTable from 'vue-good-table'
import vSelect from 'vue-select'
import * as VueGoogleMaps from 'vue2-google-maps'
import {store} from './store/store'

Vue.config.productionTip = false
Vue.use(vmodal, { dialog: true })
Vue.use(VueGoodTable)
Vue.component('v-select', vSelect)
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyBvWE_sIwKbWkiuJQOf8gSk9qzpO96fhfY'
  }
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  template: '<App/>',
  components: { App }
})
